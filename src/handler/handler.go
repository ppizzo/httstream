package handler

import (
	"log"
	"net/http"
	"net"
	"bufio"
)

const (
	endpoint = "tx1:8002"
	bufsize = 10240
)

func Handler(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request received")

	// Connect to remote endpoint
	conn, err := net.Dial("tcp", endpoint)
	if err != nil {
		log.Printf("ERROR: %s", err)
		return
	}
	defer conn.Close()
	log.Printf("Connected to %s", endpoint)

	// Set content type
	w.Header().Set("Content-Type", "video/webm")

	buffer := make([]byte, bufsize)
	reader := bufio.NewReader(conn)

	n, err := reader.Read(buffer);

	nn := 1
	for ; err == nil && nn > 0 && n > 0; n, err = reader.Read(buffer) {
		nn, _ = w.Write(buffer[:n])
		log.Printf("%d bytes written", nn)
	}
}
