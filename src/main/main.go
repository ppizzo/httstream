package main

import (
	"handler"
	"log"
	_ "fmt"
	"net/http"
)

func main() {
	log.Printf("Starting")
	http.HandleFunc("/", handler.Handler)
    log.Fatal(http.ListenAndServe(":8080", nil))
}

